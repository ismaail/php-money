<?php

namespace Database\Factories;

use App\Models\Product;
use App\Enums\ISOCurrencies;
use Bezhanov\Faker\Provider\Device;
use Bezhanov\Faker\Provider\Commerce;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class ProductsFactory
 * @package Database\Factories
 */
class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $this->faker->addProvider(new Commerce($this->faker));
        $this->faker->addProvider(new Device($this->faker));

        return [
            'name' => $this->faker->productName,
            'price' => random_int(20_000, 900_000),
            'currency' => ISOCurrencies::MAD->value,
        ];
    }
}
