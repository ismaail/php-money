{{--@php /** @var \App\Models\Product $product */ @endphp--}}
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('bootstrap.min.css') }}">
        <title>Money PHP</title>
    </head>
    <body class="bg-secondary">
        <div class="container py-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">Products</h3>
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($products as $product)
                        <li class="list-group-item">
                            {{ $product->name }}
                            <span class="float-end">
                                {{ $product->price_formatted }}
                                <small><strong>{{ $product->price->getCurrency() }}</strong></small>
                            </span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>
