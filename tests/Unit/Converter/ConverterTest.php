<?php

declare(strict_types=1);

namespace Tests\Unit\Converter;

use Money\Money;
use Money\Currency;
use Money\Converter;
use Money\CurrencyPair;
use Money\Exchange\FixedExchange;
use Money\Currencies\ISOCurrencies;

it('create currencypair with invalid conversion ratio', function () {
    $pair = CurrencyPair::createFromIso('EUR/USD .2500');

    $this->assertEquals('EUR', $pair->getBaseCurrency());
    $this->assertSame('.2500', $pair->getConversionRatio());
});

it('convert currency with exchange', function () {
    $exchange = new FixedExchange([
        'EUR' => [
            'USD' => '1.25',
        ]
    ]);

    $converter = new Converter(new ISOCurrencies(), $exchange);

    $eur100 = Money::EUR(100);
    $usd125 = $converter->convert($eur100, new Currency('USD'));

    $this->assertSame('125', $usd125->getAmount());
});

it('convert currency with pair', function () {
    $exchange = new FixedExchange([
        'EUR' => [
            'USD' => '1.00',
        ]
    ]);

    $converter = new Converter(new ISOCurrencies(), $exchange);

    $eur100 = Money::EUR(100);
    $pair = CurrencyPair::createFromIso('EUR/USD .2500');

    $usd125 = $converter->convertAgainstCurrencyPair($eur100, $pair);
    $this->assertSame('25', $usd125->getAmount());
});
