<?php

declare(strict_types=1);

namespace Tests\Unit\Currencies;

use Money\Money;
use Money\Currency;
use App\Currencies\TIFCurrencies;
use Money\Formatter\DecimalMoneyFormatter;

it('custom currency', function () {
    $money = new Money(100_005, new Currency('UTF'));
    $formatter = new DecimalMoneyFormatter(new TIFCurrencies());

    $this->assertSame('100005', $money->getAmount());
    $this->assertSame('1.00005', $formatter->format($money));
});
