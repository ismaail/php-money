<?php

declare(strict_types=1);

namespace Tests\Unit;

use Money\Money;
use Money\Currency;
use Money\Currencies\ISOCurrencies;
use Money\Parser\DecimalMoneyParser;
use Money\Formatter\IntlMoneyFormatter;
use Money\Formatter\DecimalMoneyFormatter;

it('money is created with correct value', function () {
    $money = new Money(10000, new Currency('USD'));

    $this->assertSame('10000', $money->getAmount());
});

it('round up', function (int $amount, string $percentage, string $expectedAmount) {
    $money = new Money($amount, new Currency('USD'));

    $this->assertSame('98000', $money->getAmount());

    $moneyResult = $money->multiply($percentage)->divide(100);

    $this->assertSame($expectedAmount, $moneyResult->getAmount());
})->with([
    [ 98_000, '2.00',   '1960' ], // 98000 * 2.00 %    = 19.6
    [ 98_000, '0.20',    '196' ], // 98000 * 1.96 %    = 1.96
    [ 98_000, '0.02',     '20' ], // 98000 * 0.02 %    = 0.196 => 0.20
    [ 98_000, '0.15',    '147' ], // 98000 * 0.15 %    = 1.47
    [ 98_000, '0.15714', '154' ], // 98000 * 0.15714 % = 1.53997
]);

it('format with decimal', function () {
    $money = new Money(124_599, new Currency('USD'));

    $formatter = new DecimalMoneyFormatter(new ISOCurrencies());

    $this->assertSame('1245.99', $formatter->format($money));
});

it('intl format with decimal', function () {
    $money = new Money(124_599, new Currency('USD'));
    $currencies = new ISOCurrencies();

    $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::DECIMAL);
    $moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);

    $this->assertSame('1,245.99', $moneyFormatter->format($money));
});

it('parse withdecimal', function (string $amount, string $expected) {
    $currencies = new ISOCurrencies();
    $parser = new DecimalMoneyParser($currencies);

    $money = $parser->parse($amount, new Currency('USD'));

    $this->assertSame($expected, $money->getAmount());
})->with([
    [ '120.99',  '12099' ],
    [ '120.90',  '12090' ],
    [ '120.09',  '12009' ],
    [ '120.00',  '12000' ],
    [ '120.000', '12000' ],
    [ '120.001', '12000' ],
    [ '120.005', '12001' ],
    [ '120.009', '12001' ],
    [ '120'    , '12000' ],
]);
