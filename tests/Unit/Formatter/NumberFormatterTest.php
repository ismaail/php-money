<?php

declare(strict_types=1);

namespace Tests\Unit\Formatter;

use Money\Money;
use Money\Currency;
use App\Formatter\NumberFormatter;
use Money\Currencies\ISOCurrencies;

it('custom formatter with default separators', function (int $amount, string $expected) {
    $money = new Money($amount, new Currency('USD'));
    $currencies = new ISOCurrencies();

    $moneyFormatter = new NumberFormatter($currencies);

    $this->assertSame($expected, $moneyFormatter->format($money));
})->with([
    [  3_456_124_599,  '34 561 245.99' ],
    [     56_124_599,     '561 245.99' ],
    [        124_599,       '1 245.99' ],
    [        124_500,       '1 245.00' ],
    [         10_099,         '100.99' ],
    [           1099,          '10.99' ],
    [           1000,          '10.00' ],
    [            900,           '9.00' ],
    [            199,           '1.99' ],
    [             99,           '0.99' ],
    [              9,           '0.09' ],
    [ -3_456_124_599, '-34 561 245.99' ],
    [    -56_124_599,    '-561 245.99' ],
    [       -124_599,      '-1 245.99' ],
    [       -124_500,      '-1 245.00' ],
    [        -10_099,        '-100.99' ],
    [          -1099,         '-10.99' ],
    [          -1000,         '-10.00' ],
    [           -900,          '-9.00' ],
    [           -199,          '-1.99' ],
    [            -99,          '-0.99' ],
    [             -9,          '-0.09' ],
]);
