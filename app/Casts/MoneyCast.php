<?php

declare(strict_types=1);

namespace App\Casts;

use Money\Money;
use Money\Currency;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class MoneyCast implements Castable
{
    public static function castUsing(array $arguments)
    {
        return new class implements CastsAttributes {
            /**
             * @param \App\Models\Product $model
             * @param string $key
             * @param mixed $value
             * @param array $attributes
             *
             * @return \Money\Money
             */
            public function get($model, string $key, mixed $value, array $attributes): Money
            {
                return new Money(
                    amount: $attributes['price'],
                    currency: new Currency($attributes['currency'])
                );
            }

            /**
             * @param \App\Models\Product $model
             * @param string $key
             * @param mixed $value
             * @param array $attributes
             *
             * @return mixed
             */
            public function set($model, string $key, mixed $value, array $attributes): mixed
            {
                return $value;
            }
        };
    }
}
