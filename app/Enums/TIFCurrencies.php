<?php

declare(strict_types=1);

namespace App\Enums;

enum TIFCurrencies: string
{
    case UTF = 'UTF';
    case UTFG = 'UTFG';
    case UTFG_VIP = 'UTFG-VIP';
}
