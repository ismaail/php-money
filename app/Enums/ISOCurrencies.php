<?php

declare(strict_types=1);

namespace App\Enums;

enum ISOCurrencies: string
{
    case MAD = 'MAD';
}
