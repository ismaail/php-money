<?php

declare(strict_types=1);

namespace App\Currencies;

use Traversable;
use ArrayIterator;
use Money\Currency;
use Money\Currencies;
use Money\Exception\UnknownCurrencyException;

/**
 * @see https://github.com/moneyphp/money/blob/master/src/Currencies/BitcoinCurrencies.php
 */
final class TIFCurrencies implements Currencies
{
    /**
     * @const int
     */
    private const SUBUNIT = 5;

    /**
     * Checks whether a currency is available in the current context.
     */
    public function contains(Currency $currency): bool
    {
        return null !== \App\Enums\TIFCurrencies::tryFrom($currency->getCode());
    }

    /**
     * Returns the subunit for a currency.
     *
     * @throws UnknownCurrencyException If currency is not available in the current context
     */
    public function subunitFor(Currency $currency): int
    {
        if (! $this->contains($currency)) {
            throw new UnknownCurrencyException($currency->getCode() . ' is not TIF currency.');
        }

        return self::SUBUNIT;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator();
    }
}
