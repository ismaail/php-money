<?php

declare(strict_types=1);

namespace App\Formatter;

use Money\Money;
use Money\Currencies;
use Money\MoneyFormatter;
use http\Exception\InvalidArgumentException;

class NumberFormatter implements MoneyFormatter
{
    public function __construct(
        private Currencies $currencies,
        private string $thousandsSeparator = ' ',
        private string $decimalSeparator = '.'
    ) {
        if ($thousandsSeparator === $decimalSeparator) {
            throw new InvalidArgumentException('Thousands Separator must be different than Decimal Separator.');
        }
    }

    /**
     * Formats a Money object as string.
     *
     * @throws \Money\Exception\FormatterException
     */
    public function format(Money $money): string
    {
        $valueBase = $money->getAmount();
        $negative = false;

        if ($valueBase[0] === '-') {
            $negative = true;
            $valueBase = substr($valueBase, 1);
        }

        $subunit = $this->currencies->subunitFor($money->getCurrency());
        $valueLength = strlen($valueBase);

        if ($valueLength > $subunit) {
            $formatted = substr($valueBase, 0, $valueLength - $subunit);
            $formatted = $this->chunk($formatted, $this->thousandsSeparator);

            $decimalDigits = substr($valueBase, $valueLength - $subunit);

            if ('' !== $decimalDigits) {
                $formatted .= $this->decimalSeparator . $decimalDigits;
            }
        } else {
            $formatted = '0' . $this->decimalSeparator . str_pad('', $subunit - $valueLength, '0') . $valueBase;
        }

        if (true === $negative) {
            $formatted = '-' . $formatted;
        }

        return $formatted;
    }

    private function chunk(string $numbers, string $separator): string
    {
        $numbers = strrev($numbers);

        $numbers = trim(chunk_split($numbers, 3, $separator));

        return strrev($numbers);
    }
}
