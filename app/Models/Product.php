<?php

declare(strict_types=1);

namespace App\Models;

use App\Casts\MoneyCast;
use App\Formatter\NumberFormatter;
use Money\Currencies\ISOCurrencies;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    /**
     * @var array<string, class-string|string>
     */
    protected $casts = [
        'price' => MoneyCast::class,
        'currency' => \App\Enums\ISOCurrencies::class,
    ];

    public function priceFormatted(): Attribute
    {
        return new Attribute(
            get: fn() => (new NumberFormatter(new ISOCurrencies()))->format($this->getAttribute('price')),
        );
    }
}
