.PHONY: up start stop down reboot artisan artisan-debug migrate migrate\:fresh migrate\:rollback seed nginx\:check nginx\:reload fpm\:reload composer ide-helper cache-clear clear-logs tests supervisor-update deploy fix-permissions

# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PWD := $(dir $(MAKEPATH))
CONTAINER_FPM := "moneyphp_fpm"
CONTAINER_WEB := "moneyphp_web"
CONTAINER_SUPERVISOR := "moneyphp_supervisor"
UID := 1000

up:
	docker-compose up -d

start:
	docker-compose start

stop:
	docker-compose stop

down:
	docker-compose down

reboot:
	docker-compose down && docker-compose up -d

cmd=""
artisan:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		php artisan $(cmd) \
		2>/dev/null || true

cmd=""
host?="127.0.0.1"
artisan-debug:
	docker exec -it \
		-u $(UID) \
		-e XDEBUG_CONFIG="mode=debug client_host=$(host) client_port=9000 start_with_request=yes" \
		-e PHP_IDE_CONFIG="serverName=localhost" \
		$(CONTAINER_FPM) \
		php artisan $(cmd) -vvv \
		2>/dev/null || true

migrate:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		php artisan migrate \
		2>/dev/null || true

migrate\:fresh:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		php artisan migrate:fresh \
		2>/dev/null || true

migrate\:rollback:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		php artisan migrate:rollback \
		2>/dev/null || true

seed:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		php artisan db:seed \
		2>/dev/null || true

nginx\:check:
	docker exec -it $(CONTAINER_WEB) nginx -t 2>/dev/null || true

nginx\:reload:
	docker kill -s HUP $(CONTAINER_WEB) 2>/dev/null || true

fpm\:reload:
	docker exec -it $(CONTAINER_FPM) kill -USR2 1

cmd=""
composer:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		composer $(cmd) \
		2>/dev/null || true

composer\:update:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		composer update -o \
		2>/dev/null || true

ide-helper:
	docker exec -it -u $(UID) $(CONTAINER_FPM) php artisan ide-helper:generate 2>/dev/null || true && \
	docker exec -it -u $(UID) $(CONTAINER_FPM) php artisan ide-helper:models --nowrite 2>/dev/null || true

cache-clear:
	docker exec -it -u $(UID) $(CONTAINER_FPM) php artisan cache:clear 2>/dev/null || true && \
	docker exec -it -u $(UID) $(CONTAINER_FPM) php artisan view:clear 2>/dev/null || true && \
	docker exec -it -u $(UID) $(CONTAINER_FPM) php artisan route:clear 2>/dev/null || true && \
	docker exec -it -u $(UID) $(CONTAINER_FPM) php artisan config:clear 2>/dev/null || true

clear-logs:
	docker exec -it $(CONTAINER_FPM) truncate -s 0 storage/logs/*.log 2>/dev/null || true

tests:
	docker exec -it \
		-u $(UID) \
		$(CONTAINER_FPM) \
		php ./vendor/phpunit/phpunit/phpunit --do-not-cache-result --configuration ./phpunit.xml \
		2>/dev/null || true

supervisor-update:
	docker exec -it $(CONTAINER_SUPERVISOR) supervisorctl reread 2>/dev/null || true && \
	docker exec -it $(CONTAINER_SUPERVISOR) supervisorctl update 2>/dev/null || true

fix-permissions:
	docker exec -i $(CONTAINER_FPM) chown -R 1000:100 ./storage/logs 2>/dev/null || true && \
	docker exec -i $(CONTAINER_FPM) find ./vendor -type d -exec chmod 755 {} \; 2>/dev/null || true && \
	docker exec -i $(CONTAINER_FPM) find ./vendor -type f -exec chmod 644 {} \; 2>/dev/null || true

deploy:
	envoy run deploy
